from django.shortcuts import redirect
from django.urls import reverse_lazy
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todolist_list"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todolist_detail"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def form_valid(self, form):
        todo = form.save(commit=False)
        todo.owner = self.request.user
        todo.save()
        form.save_m2m()
        return redirect("todo_list_detail", pk=todo.id)

    def get_queryset(self):
        return TodoList.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todo_items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todo_items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
